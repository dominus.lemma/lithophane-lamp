import digitalio
import pulseio
import board
import time
import math
import rotary_encoder

red = pulseio.PWMOut(board.SCK, frequency=5000, duty_cycle=0)
green = pulseio.PWMOut(board.MOSI, frequency=5000, duty_cycle=0)
blue = pulseio.PWMOut(board.MISO, frequency=5000, duty_cycle=0)
white = pulseio.PWMOut(board.RX, frequency=5000, duty_cycle=0)
pwmPins = [red, green, blue, white]

# rotary RGB=A2,A3,A4
rotaryRed = digitalio.DigitalInOut(board.A2)
rotaryRed.switch_to_output()
rotaryGreen = digitalio.DigitalInOut(board.A3)
rotaryGreen.switch_to_output()
rotaryBlue = digitalio.DigitalInOut(board.A4)
rotaryBlue.switch_to_output()
rotaryColorPins = [rotaryRed, rotaryGreen, rotaryBlue]

rotaryToggle = digitalio.DigitalInOut(board.A1)
rotaryToggle.switch_to_input(pull=digitalio.Pull.DOWN)

rotaryEncoder = rotary_encoder.RotaryEncoder()
rotaryAPin = digitalio.DigitalInOut(board.D5)
rotaryAPin.switch_to_input(pull=digitalio.Pull.UP)
rotaryBPin = digitalio.DigitalInOut(board.SCL)
rotaryBPin.switch_to_input(pull=digitalio.Pull.UP)

modeSwitch = digitalio.DigitalInOut(board.SDA)
modeSwitch.switch_to_input(pull=digitalio.Pull.DOWN)


# i = an integer between 0 and count
# frequency = number of cycles in "count" steps (works better with int values)
# count = total number of steps in a single period
def getDutyCycle(i, frequency, count):
    step = (i/count) * frequency * (2 * math.pi)
    factor = (math.sin(step) + 1)/2
    duty_cycle = 65535 * factor
    if duty_cycle > 65535:
        duty_cycle = 65535
    elif duty_cycle < 640:
        duty_cycle = 0

    return int(duty_cycle)

_colorDutyCycles = [0, 0, 0, 0]
colorList = ['red', 'green', 'blue', 'white']
currentColorIndex = 3
rotaryToggleState = False

def setRotaryColor(colorIndex):
    global rotaryColorPins
    #RGB=A2,A3,A4
    if colorIndex == 3:
        for pin in rotaryColorPins:
            pin.value = False
    else:
        for i in range(3):
            rotaryColorPins[i].value = (colorIndex != i)
setRotaryColor(3)

def handleEvents():
    global rotaryToggleState
    global currentColorIndex
    global rotaryColorPins
    global modeSwitch
    global automaticColorCycle
    global _colorDutyCycles

    if modeSwitch.value:
        #automatic color cycle mode
        setRotaryColor(-1)
        automaticColorCycle = True
    else:
        #manual color cycle mode, use rotaryToggle to cycle colors and encoder to change value
        automaticColorCycle = Falses
        if not rotaryToggle.value and rotaryToggleState != rotaryToggle.value:
            # cycle color channel only on the rising edge
            rotaryToggleState = rotaryToggle.value
            currentColorIndex = (currentColorIndex + 1) % 4
            time.sleep(.1)

        setRotaryColor(currentColorIndex)
        rotaryToggleState = rotaryToggle.value
        increment = rotaryEncoder.turn(rotaryAPin.value, rotaryBPin.value)

        currentPin = pwmPins[currentColorIndex]
        currentDutyCycle = _colorDutyCycles[currentColorIndex]
        if increment != 0:
            print(increment)
        if increment == 16:
            currentDutyCycle = min((currentDutyCycle + 1024), 65535)
        elif increment == 32:
            currentDutyCycle = max((currentDutyCycle - 1024), 0)

        currentPin.duty_cycle = currentDutyCycle
        _colorDutyCycles[currentColorIndex] = currentDutyCycle

# number of iterations per period, prefer changing this over delay to modify period length
count = 16000
# milliseconds to wait between changing duty cycle, prefer using count to change period
delay = .01
automaticColorCycle = modeSwitch.value
staticColorSet = modeSwitch.value
while True:
    for i in range(count):
        if automaticColorCycle:
            # autmoatically cycle colors
            staticColorSet = False
            white.duty_cycle = getDutyCycle(i, 1, count)
            red.duty_cycle = getDutyCycle(i, 2, count)
            blue.duty_cycle = getDutyCycle(i, 3, count)
            green.duty_cycle = getDutyCycle(i, 5, count)
            time.sleep(delay)
        elif not staticColorSet:
            # we're in static color mode but only set the color if we have to
            staticColorSet = True
            for i in range(4):
                pwmPins[i].duty_cycle = _colorDutyCycles[i]

        handleEvents()