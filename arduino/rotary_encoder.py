class RotaryEncoder:
    _state = 0x0

    #0x10 = CW
    #0x20 = CCW
    _stateTable = [\
        [0x0, 0x4, 0x2, 0x0],\
        [0x0 | 0x10, 0x1, 0x0, 0x3],\
        [0x0, 0x0, 0x2, 0x3],\
        [0x0, 0x1, 0x2, 0x3],\
        [0x0, 0x4, 0x0, 0x6],\
        [0x0 | 0x20, 0x0, 0x5, 0x6],\
        [0x0, 0x4, 0x5, 0x6],\
    ]


    def _translatePinstate(self, a, b):
        if a and b:
            return 3
        elif a and not b:
            return 2
        elif not a and b:
            return 1
        else:
            return 0

    def turn(self, a, b):
        pinstate = self._translatePinstate(a, b)
        self._state = self._stateTable[self._state & 0xF][pinstate]
        return self._state & 0x30